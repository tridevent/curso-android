#pragma strict
var velocidadX:float = 10.0;
var velocidadY:float = 10.0;
function Start () {

}

function Update () {
 var dir : Vector3 = Vector3.zero;
 dir.x = Input.acceleration.x;
 dir.y = Input.acceleration.y;

 if (dir.sqrMagnitude > 1)
  dir.Normalize();

 // esta instruccion es para que se mueve 10 unidades por segundo y no por frame.
 dir *= Time.deltaTime;
 // mover el objeto
 transform.Translate (dir.x *velocidadX, dir.y *velocidadY, 0);
}